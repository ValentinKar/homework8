<?php 
error_reporting(E_ALL);        //вывести на экран все ошибки
require_once 'login.php'; 
session_start(); 

$content = file_get_contents("tests.json");    //загружаю json-данные из файла
$tests = json_decode($content, true);     //json-данные записываю в массив
function test()   
{         // Проверяю, передан ли номер теста методом POST
	if (isset($_POST) && $_SERVER['REQUEST_METHOD'] === 'POST' )
	{
		return true;
	}
	else 
	{
		return false;
	}
};

if (test() )  {
	$i = 1;
	$new_tests[] = "";
	foreach ($tests as $k => $a) { 
		if (isset($_POST['t'.$i])) {  //если передан номер теста методом POST
			unset($tests[$k]);       //тогда удаляю его из массива
		}
		$i = $i + 1;
	}

$content = json_encode($tests,JSON_UNESCAPED_UNICODE);  // кодирую массив, полученный в предыдущей строке в json формат
file_put_contents("tests.json", $content);     // записываю результат в json файл
echo "тест удален";
echo "<br /><a href=\"list.php\">Вернуться к списку тестов</a>";
die;
};

if (!isLog())  {  
	header('HTTP/1.0 404 Not Found');  // Если вход выполнен по прямой ссылке, тогда 404 ошибка
	die; 
} 

?>
<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="UTF-8">
<title>Удаление теста</title>
</head>
<body>
<h1>Список тестов для удаления: </h1>

<form method="POST">
	<?php 
	$i = 1;
	foreach ($tests as $key => $array)  
	{ 
	?>
		<br />
		<input type="checkbox" name='t<?php 
		echo $i;      // номер варианта ответа, пойдет в метод POST
		?>'>
		<?php  echo $key; ?>
	<?php  
	$i++;
	}; 
	?>
<br />
<br />
<input type="submit" value="Удалить выбранные тесты" >
</form>

</body>
</html>