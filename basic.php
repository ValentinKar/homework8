<?php 
mb_internal_encoding('utf-8');  //позволяет использовать все ф-ции для кириллицы
error_reporting(E_ALL);        //вывести на экран все ошибки
require_once 'login.php'; 

if (isLog())  
{  
	header('Location: list.php'); 
	die; 
} 
else 
{ 
    header('WWW-Authenticate: Basic realm="x"'); 
    die; 
}