<?php 
error_reporting(E_ALL);        //вывести на экран все ошибки


function isNumber()   
{         // Проверяю, передан ли номер теста методом гет
	if ($_SERVER['REQUEST_METHOD'] === 'GET' && !empty($_GET['number']) )
	{
		return true;
	}
	else 
	{
		return false;
	}
};

function isQuestion()
{        // если в методе GET передан первый вопрос теста - т.е. если тест выполнен
	if ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['q_1']) )
	{
		return true;
	}
	else 
	{
		return false;
	}
};


function rightTests($json)
{        // проверка правильности каждого вопроса теста
	$i = 1; 
	foreach ($_GET as $quest => $answ) // читаю переданные ответы после выполнения теста  
	{
		if ($quest !== "number" && $quest !== "user_name") // пропускаю первый и последний ключ массива $_GET т.к. они передают номер теста и имя пользователя, а мне нужны ответы
		{
			if ($json['block_'.$i]['radio'][$answ] === $json['block_'.$i]['answer']) // если переданный номер ответа совпадает с правильным ответом из json
				{ 
					$answer[$i] = true;  // переменная для одного вопроса, которая описывает правильность ответа
				}
			else 
				{ 
				$answer[$i] = false;  // переменная для одного вопроса, которая описывает правильность ответа
				}; 
			$i++;
		};
	};
return $answer;
};


function rating($arr)
{        // ф-ция определяет оценку
$k = 0; // кол-во правильных ответов
foreach ($arr as $key => $value) // 
	{
		if ($value) {
			$k++;
		};
	}
$x = $k / $key;    // Доля правильных ответов
if ($x >= 0.9) { $rat = 'отлично'; };
if ($x >= 0.75 && $x < 0.9) { $rat = 'хорошо'; };
if ($x >= 0.5 && $x < 0.75) { $rat = 'удовлетворительно'; };
if ($x < 0.5) { $rat = 'неудовлетворительно'; };
return $rat;
};  

function isRating() 
{    // Проверка, выполнен ли тест на положительную оценку
	if (isset($_GET['rating']) && $_GET['rating'] !== "неудовлетворительно" && $_GET['rating'] !== "" ) {  $ansver = true; 	}
	else { 	$ansver = false; 	}
return $ansver; 
};