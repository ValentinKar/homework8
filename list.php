<?php 
error_reporting(E_ALL);        //вывести на экран все ошибки
require_once 'login.php'; 
include_once 'function.php'; 
session_start(); 

$content = file_get_contents("tests.json");    //загружаю json-данные из файла
$tests = json_decode($content, true);     //json-данные записываю в массив
$a_href = "";

if (isGuest() )  { 
$user = htmlspecialchars($_POST['name']);        // переменная с именем пользователя
setcookie('name', $user, time() + (60*TIME_LIVE) );
} 

if (isLog() )  {  // если прошла авторизация
	setcookie('name', 'admin', time() + (60*TIME_LIVE) );
$a_href = "
<a href=\"admin.php\">Добавить тест</a>
<br />
<a href=\"delete.php\">Удалить тест</a>
";
} 

if (!isGuest() && !isLog() && !isset($_COOKIE['name']) )  { 
	header('HTTP/1.0 403 Forbidden');  // нет аунтефикации пользователя
	die;
} 

?>
<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="UTF-8">
<title>Удаление теста</title>
</head>
<body>

<?php if (isset($_GET['rating']) )  { ?>
Оценка за выполненный тест: <?php  echo $_GET['rating']; }  ?>
<br/>

<?php if (isRating() )  { ?>
<img src='<?php   // картинка с сертификатом
echo "module_img/certificate.php?name=".$_COOKIE['name']."&theme=".$_GET['theme']."&rating=".$_GET['rating']; 
?>' width="220" height="164" border="1px" alt="рамка для сертификата, получаемого по результатам прохождения теста" title="что-бы скачать, кликни правой кнопкой и нажми сохранить картинку как"> <?php };  ?>

<h1>Список тестов: </h1>
<ol>
	<?php 
	$i = 1;
	foreach ($tests as $key => $array)  
	{ 
	?>
		<a href="test.php?number=<?php echo $i; ?>">
		<li><?php  echo $key; ?></li>
		</a>
	<?php  
	$i++;
	}; 
	?>
</ol>
<?php echo $a_href; ?>
</body>
</html>