<?php 
error_reporting(E_ALL);        //вывести на экран все ошибки
include_once 'function.php';       // подгружаю файл, содержащий функции

$t_redir = '';     //переменная в которой будет текст редиректа
$url_redir = '';     //переменная, в которой будет адрес редиректа
$content = file_get_contents("tests.json");    //загружаю json-данные из файла
$tests = json_decode($content, true);     //json-данные записываю в массив
$rating = '';     //переменная в которой будет результат теста


if (!isset($_COOKIE['name']))
{    // если пользователь зашел по прямой ссылке или выплнял тест дольше жизни cookie
echo 'время выполнения теста ' . TIME_LIVE . ' мин.'; 
echo '<br />'; 
echo 'зайдите на index.php и введите свое имя или авторизуйтесь'; 
exit; 
};

// vvvvvv   Проверяю, передан ли номер теста методом гет
if (isNumber())  {
	$i = 1; 
	foreach ($tests as $k => $a) { 
		if ($i === (integer)$_GET['number']) {  //если номер теста по порядку и переданный номер совпадают
		$theme = $k; // Опрелеляю тему теста, если номер теста передан методом гет
		}
		$i = $i + 1;
	}
}
else { 
	header('HTTP/1.0 404 Not Found');  // Если номера теста нет, тогда 404 ошибка
	exit;  // Если номера теста нет, тогда выхожу из программы
};
// ^^^^^^   Проверяю, передан ли номер теста методом гет

if (empty($theme) )  {  // Проверяю, есть ли тест с переданным по GET номером
	header('HTTP/1.0 404 Not Found');  // Если теста c с переданным номером отсутствует, тогда 404 ошибка
	exit;  // Если теста c с переданным номером отсутствует, тогда выхожу из программы
}
$test = $tests[$theme];   // Выбираю тест, согласно переданному номеру 

if (isQuestion())   // если в методе GET передан первый вопрос теста - т.е. если тест выполнен
{ 
		if (empty($_COOKIE['name']))    // если пользователь не ввел свое имя
		{
		echo 'не определено имя пользователя'; 
		exit; 
		};
	$right = rightTests($test);      // массив булевых переменных, хар-щих правильность ответов
	$rating = rating($right);        // общая оценка за выполненный тест
$user = htmlspecialchars($_COOKIE['name']);        // переменная с именем пользователя
header("Location:  list.php?name=".$user."&theme=".$theme."&rating=".$rating); 
}; 

?>
<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="UTF-8">
<title>Test</title>
<style>
body {
 font-family: sans-serif;
 font-size: 15px;
}
table {
border: 1px solid black; 
padding: 4px;
}
td {
border: 1px solid black; 
padding: 4px;
}
</style>
</head>
<body>
<h1>Тест: <?php echo $theme; ?></h1>

<form action="test.php" method="GET">
<input type="hidden" name="number" value="<?php echo $_GET['number']; ?>">
<?php 
$i = 1;   // счетчик, использую для вывода вопросов в метод GET
foreach ($test as $keys => $array)  // начинаю цикл для массива вопросов
{ 
	echo $array['question'];    // Вывожу вопрос в тело страницы
	if (!empty($right[$i]) && $right[$i] === true) { 
	echo " - правильно";   // если выбран правильный вариант ответа
	}
	if (isset($right[$i]) && $right[$i] === false) { 
	echo " - неправильно";    // если выбран неправильный вариант ответа
	}; 
?>
	<br/>
		<?php  // для одного вопроса и нескольких вариантов ответа начинаю цикл
		foreach ($array['radio'] as $key => $answer) 
		{ 
		?>
		<label>
		<input type="radio" name='<?php 
		echo 'q_'.$i;     // q_1 - переменная, пойдет в метод GET, обозначает номер вопроса
		?>' value='<?php 
		echo $key;      // номер варианта ответа, пойдет в метод GET
		?>'>
		<?php echo $answer; ?>    <!-- вариант ответа-->
		</label>
		<br/>
		<?php 
		};   // для одного вопроса и нескольких вариантов ответа завершаю цикл
		?>
	<br/><br/>
<?php 
$i++; 
};    // завершаю цикл для массива вопросов
?>
<input type="submit" value="Отправить" >
</form>

</body>
</html>